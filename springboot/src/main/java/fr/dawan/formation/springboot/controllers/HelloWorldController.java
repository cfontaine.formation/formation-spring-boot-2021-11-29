package fr.dawan.formation.springboot.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {
    
    @Value("${message}")
    private String msg;
    
    @RequestMapping("/")
    public String home() {
        return msg; //"Hello World";
    }
}
