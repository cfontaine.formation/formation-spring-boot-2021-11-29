package fr.dawan.formation.springboot.repositories;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.dawan.formation.springboot.entities.Article;

public interface ArticleRepository extends JpaRepository<Article, Long> {
    
     List<Article>findByPrixLessThanOrderByPrixAsc(double valLimit);
     
     @Query(value="SELECT a FROM Article a WHERE a.prix<:valLimit")
     List<Article>findPrixInferieur(@Param("valLimit")double max);
     
     @Query(nativeQuery = true,value="SELECT * FROM articles WHERE prix<:valLimit")
     List<Article>findPrixInferieurSQL(@Param("valLimit")double max);
         
     Page<Article>findByPrixLessThan(double valLimit,Pageable pageable);
     
     List<Article>findTop3ByPrixLessThanOrderByPrixAsc(double valLimit);
}
