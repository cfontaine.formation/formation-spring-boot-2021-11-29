package fr.dawan.formation.springboot.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table (name="fournisseurs")
public class Fournisseur extends AbstractEntity implements Serializable {

    
    private static final long serialVersionUID = 1L;
    
    @ManyToMany
    private List<Article> articles =new ArrayList<>();
    
    private String nom;

    public Fournisseur(String nom) {
        super();
        this.nom = nom;
    }

    public Fournisseur() {
        super();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Fournisseur [nom=" + nom + "]";
    }
    
    
    
}
