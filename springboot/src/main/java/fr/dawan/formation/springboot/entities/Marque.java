package fr.dawan.formation.springboot.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
@Entity
@Table(name="marques")
public class Marque extends AbstractEntity implements Serializable {

    

    private static final long serialVersionUID = 1L;
    
    private String nom;

    @OneToMany(mappedBy = "marque")
    private List<Article> articles=new ArrayList<>();
    
    public Marque() {
        super();
    }

    public Marque(String nom) {
        super();
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "Marque [nom=" + nom + "]";
    }
    
}
