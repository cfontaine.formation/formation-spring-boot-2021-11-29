package fr.dawan.formation.springboot.entities;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="personnes")
public class Personne implements Serializable {

    @Id
    private long id;
    
    
    private String prenom;
    
    private String nom;
    
    @Embedded
    private Adresse adresse;

    public Personne() {
        super();
    }

    public Personne(String prenom, String nom) {
        super();
        this.prenom = prenom;
        this.nom = nom;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
    
    
}
