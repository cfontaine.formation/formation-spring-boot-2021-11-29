package fr.dawan.formation.springboot.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import fr.dawan.formation.springboot.enums.Conditionement;



@Entity
@Table(name="articles")
public class Article extends AbstractEntity implements Serializable  {

    private static final long serialVersionUID = 1L;
    
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private long id; 
    
//    @Version
//    private int version;
    @ManyToOne
    private Marque marque;
    
    @ManyToMany(mappedBy = "articles",fetch=FetchType.EAGER)
    List<Fournisseur> fournisseurs=new ArrayList<>();

    @Column(length = 100,nullable = false)
    private String description;
    
    @Column(nullable = false)
    private double prix;
    
    @Enumerated(EnumType.STRING)
    private Conditionement conditionnement;
    
    @ElementCollection(targetClass = Integer.class)
    @CollectionTable(name="article_etoile")
    private List<Integer> etoile;

    public Article() {
        super();
    }

    public Article(String description, double prix, Conditionement conditionnement) {
        super();
        this.description = description;
        this.prix = prix;
        this.conditionnement = conditionnement;
    }


//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrix() {
        return prix;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public Marque getMarque() {
        return marque;
    }

    public void setMarque(Marque marque) {
        this.marque = marque;
    }

    public Conditionement getConditionnement() {
        return conditionnement;
    }

    public void setConditionnement(Conditionement conditionnement) {
        this.conditionnement = conditionnement;
    }

    public List<Integer> getEtoile() {
        return etoile;
    }

    public void setEtoile(List<Integer> etoile) {
        this.etoile = etoile;
    }

    @Override
    public String toString() {
        return "Article [id=" + super.getId() + ", description=" + description + ", prix=" + prix + "]";
    }

}
