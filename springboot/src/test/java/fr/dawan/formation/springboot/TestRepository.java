package fr.dawan.formation.springboot;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import fr.dawan.formation.springboot.entities.Article;
import fr.dawan.formation.springboot.repositories.ArticleRepository;

@SpringBootTest
public class TestRepository {

    @Autowired
    ArticleRepository repo;
    
    @Test
    void testRepoArticle() {
        List<Article> l=repo.findTop3ByPrixLessThanOrderByPrixAsc(50.0);
        l.forEach(System.out::println);
        
        Page<Article> p=repo.findByPrixLessThan(100.0, PageRequest.of(1, 3));
        System.out.println(p.getTotalElements() +  " " + p.getTotalPages());
        p.forEach(System.out::println);
    }
    
}
