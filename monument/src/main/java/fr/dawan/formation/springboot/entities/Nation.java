package fr.dawan.formation.springboot.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="nations")
public class Nation extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false,length = 100)
    private String nom;
    
    @OneToMany(mappedBy = "nation")
    private List<Monument> monuments=new ArrayList<>();
    
    public Nation() {
    }

    public Nation(String nom) {
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Nation [ " + super.toString() + " Nom=" + nom + "]";
    }
      
}
