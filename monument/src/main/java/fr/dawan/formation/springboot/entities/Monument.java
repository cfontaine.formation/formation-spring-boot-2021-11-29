package fr.dawan.formation.springboot.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="monuments")
public class Monument extends AbstractEntity {

    private static final long serialVersionUID = 1L;

    @Column(nullable = false)
    private String nom;
    
    @Column(nullable = false)
    private int anneeConstruction;
    
    @OneToOne
    private Coordonee cordonee;
    
    @ManyToOne
    private Nation nation;

    public Monument() {
    }

    public Monument(String nom, int anneeConstruction) {
        this.nom = nom;
        this.anneeConstruction = anneeConstruction;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getAnneeConstruction() {
        return anneeConstruction;
    }

    public void setAnneeConstruction(int anneeConstruction) {
        this.anneeConstruction = anneeConstruction;
    }

    @Override
    public String toString() {
        return "Monument [nom=" + nom + ", anneeConstruction=" + anneeConstruction + ", id=" + super.toString() + "]";
    }

}

