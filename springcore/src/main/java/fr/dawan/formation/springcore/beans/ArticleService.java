package fr.dawan.formation.springcore.beans;

import java.io.Serializable;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component(value = "serviceA")
@Scope("prototype")
public class ArticleService implements Serializable {

    private static final long serialVersionUID = 1L;

//    @Autowired
//    @Qualifier("daoB")
    private ArticleDao dao;

    public ArticleService() {
        System.out.println("Constructeur defaut Service");
    }
    
    @Autowired 
    public ArticleService(@Qualifier("daoB")ArticleDao dao) {
        System.out.println("Constructeur 1 param Service");
        this.dao = dao;
    }

    public ArticleDao getDao() {
        return dao;
    }

   // @Qualifier("daoB")
    public void setDao(/*@Qualifier("daoB")*/ ArticleDao dao) {
        System.out.println("setter");
        this.dao = dao;
    }

    @Override
    public String toString() {
        return "ArticleService [dao=" + dao +" " + super.toString()+" ]";
    }

}
