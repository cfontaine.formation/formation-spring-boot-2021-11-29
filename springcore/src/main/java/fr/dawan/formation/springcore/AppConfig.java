package fr.dawan.formation.springcore;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import fr.dawan.formation.springcore.beans.ArticleDao;
import fr.dawan.formation.springcore.beans.ArticleService;

@Configuration
@ComponentScan(basePackages = "fr.dawan.formation.springcore")
public class AppConfig {

    @Bean
    public ArticleDao daoA() {
        return new ArticleDao("data");
    }
    
    @Bean
    public ArticleDao daoB() {
        return new ArticleDao("dataB");
    }

//    @Bean
//    public ArticleService serviceA(ArticleDao daoA) {
//        return new ArticleService(daoA);
//    }
    
//    @Bean
//    public ArticleService serviceA() {
//        return new ArticleService();
//    }

}
