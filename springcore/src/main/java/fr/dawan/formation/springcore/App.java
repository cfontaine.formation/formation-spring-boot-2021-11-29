package fr.dawan.formation.springcore;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.dawan.formation.springcore.beans.ArticleDao;
import fr.dawan.formation.springcore.beans.ArticleService;


public class App 
{
    public static void main( String[] args )
    {
        ApplicationContext ctx= new AnnotationConfigApplicationContext(AppConfig.class);
        System.out.println("__________________________________");   
        ArticleDao daoA=ctx.getBean("daoA",ArticleDao.class);
        ArticleDao daoB=ctx.getBean("daoB",ArticleDao.class);
        ArticleService service=ctx.getBean("serviceA",ArticleService.class);
        System.out.println(daoA);
        System.out.println(daoB);
        System.out.println(service);
        ArticleService service2=ctx.getBean("serviceA",ArticleService.class);
        System.out.println(service2);
    }
}
